ILSP LT services provided by ILSP/ARC

Further information on ILSP LT services:

* Dimitris Galanis, galanisd@athenarc.gr: LT services integration
* Sokratis Sofianopoulos, s_sofian@athenarc.gr: Machine translation services 
    * ILSP Machine Translation Service for English to modern Greek
    * ILSP Machine Translation Service for modern Greek to English
* Maria Pontiki, mpontiki@athenarc.gr: Information extraction services
    * Named Entity Recognizer for Greek
    * ILSP Named Entity Recognizer for English
    * ILSP event extractor for physical attacks in Greek
    * ILSP event extractor for protest events in Greek
    * ILSP opinion mining for customer reviews in Greek

